# AeFrontendTexteditorAngularSkeleton

Testing task for AgileEngine

Texteditor

To launch application follow the instructions:

1. Clone the project

2. Run `npm i` in the root directory of the project

3. Run `ng s` in the directory

4. Now application should be available by url `http://localhost:4200/`


Note: for launching this project Node.js, npm and @angular/cli should be installed on your machine
