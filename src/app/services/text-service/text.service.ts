import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

interface ISynonymObject {
  score: number;
  word: string;
}

@Injectable()
export class TextService {

  // Events for text formatting
  public onFormattingApply: EventEmitter<'italic' | 'bold' | 'underline'> = new EventEmitter();
  public onSelectWord: EventEmitter<string> = new EventEmitter();
  public onSynonymChoose: EventEmitter<string> = new EventEmitter();

  constructor(private http: HttpClient) {}

  getMockText() {
    return new Promise<string>(function (resolve) {
      resolve('A year ago I was in the audience at a gathering of designers in San Francisco. ' +
        'There were four designers on stage, and two of them worked for me. I was there to support them. ' +
        'The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. ' +
        'What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, ' +
        'that modern design problems were very complex. And we ought to need a license to solve them.');
    });
  }

  /**
   * Request synonyms for particular word
   * @param {string} word
   * @returns {Promise<string[]>}
   */
  getSynonyms(word: string): Promise<string[]> {
    return this.http.get('https://api.datamuse.com/words?rel_syn=' + word)
      .toPromise()
      .then((results: Array<ISynonymObject>) => {
        return results.map(r => r.word);
      });
  }
}
