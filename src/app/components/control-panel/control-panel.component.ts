import { Component } from '@angular/core';
import {TextService} from '../../services/text-service/text.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent {

  synonyms: Array<string> = [];

  constructor(private textService: TextService) {
    this.textService.onSelectWord.subscribe(word => {
      this.synonyms = [];
      this.requestSynonyms(word);
    });
  }

  /**
   * Make a request to get synonyms of particular word
   * @param {string} word
   */
  private requestSynonyms(word: string) {
    this.textService.getSynonyms(word).then(result => {
      this.synonyms = result;
    });
  }

  /**
   * Fire event for changing formatting
   * @param {"bold" | "italic" | "underline"} type
   */
  applyFormatting(type: 'bold' | 'italic' | 'underline') {
    this.textService.onFormattingApply.emit(type);
  }

  /**
   * Fire event for replacing word with the synonym
   * @param {string} synonym
   */
  replaceWithSynonym(synonym: string) {
    this.textService.onSynonymChoose.emit(synonym);
  }
}
