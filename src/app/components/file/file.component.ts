import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { TextService } from '../../services/text-service/text.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit, OnDestroy {

  private currentSelection: Selection;
  private selectedWordWrap: HTMLElement;
  private subscriptions: Array<Subscription> = [];
  @ViewChild('textField') textField: ElementRef;
  text: string;

  constructor(private textService: TextService) {
    // Subscribe on text formatting changes
    const formattingSubscription = this.textService.onFormattingApply.subscribe(this.applyFormatting.bind(this));

    // Subscribe on synonyms replacing
    const synonymSubscription = this.textService.onSynonymChoose.subscribe(this.replaceWithSynonym.bind(this));

    this.subscriptions.push(formattingSubscription, synonymSubscription);
  }

  ngOnInit() {
    this.textService.getMockText().then((result) => {
      this.text = result;
    });
  }

  /**
   * Handle double click event
   * @param e
   */
  handleDoubleClick(e) {
    this.currentSelection = window.getSelection();
    if (this.currentSelection.isCollapsed ||
      !this.currentSelection.toString().trim() ||
      !this.textField.nativeElement.contains(e.target)
    ) {
      this.currentSelection = null;
      return;
    }
    this.selectedWordWrap = e.target === this.textField.nativeElement ? null : e.target;
    this.textService.onSelectWord.emit(this.currentSelection.toString());
  }

  /**
   * Apply text formatting
   * @param formattingType
   */
  private applyFormatting(formattingType) {
    if (!this.currentSelection) { return; }
    this.wrapSelectedWord();
    const style = this.selectedWordWrap.style;
    switch (formattingType) {
      case 'bold':
        style.fontWeight = style.fontWeight ? null : 'bold';
        break;
      case 'italic':
        style.fontStyle = style.fontStyle ? null : 'italic';
        break;
      case 'underline':
        style.textDecoration = style.textDecoration ? null : 'underline';
        break;
    }
  }

  /**
   * Replace word with the synonym
   * @param {string} synonym
   */
  private replaceWithSynonym(synonym: string) {
    this.wrapSelectedWord();
    this.selectedWordWrap.innerText = synonym;
  }

  /**
   * Add wraper to the selected word
   * to apply styles on it
   */
  private wrapSelectedWord() {
    if (!this.selectedWordWrap) {
      this.selectedWordWrap = document.createElement('span');
      const range = this.currentSelection.getRangeAt(0).cloneRange();
      range.surroundContents(this.selectedWordWrap);
      this.currentSelection.removeAllRanges();
      this.currentSelection.addRange(range);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
